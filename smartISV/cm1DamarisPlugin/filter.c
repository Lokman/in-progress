#ifndef FILTER_C
#define FILTER_C

#include <math.h>
#include <string.h>
#include <time.h>

//#define EPSILON 0.8
#define INTERVAL_NUMBER_DEFAULT 0.5
#define INTERVAL_SIZE 0.05

extern "C" {

extern int iter;

long quickSortPartition(float* values, long left, long right, long pIndex);
int quickSort(float* values, long min, long max);
int calcProbasHist(float* values, long size, float intervalSize, long intervalNumber, double** probas, long* num);
//int calcProbasHist(float* values, long size, double* probas, long* num);
int calcProbasSum(float* values, long size, double* probas, long* num);
double calculateShanon(double *probas, long num);
double calculateTsallis(double *probas, long num, double entropyIndex);
double calculateNCV(float* values, long size, double* cv, double* stdDev, double *mean, int *normalized);
int voxelize(int num_dim, int* sizes, size_t type_size, const char* in_buf, char* out_buf);
//int isInterstingCV(float* values, long size, double epsilon, float *mean);
int isInterstingCV(float* values, long size, double epsilon);
int isInterstingShanon(float* values, long size, double epsilon);
int isInterstingShanonV2(float* values, long size, double epsilon);
int smooth_mean(float *data, int size, float mean);


int voxelize(int num_dim, int* sizes, size_t type_size, const char* in_buf, char* out_buf)
{
	int din = 0, dout = 0;
	int i = 0;
	
	if ( num_dim==1 ){
	  memcpy(out_buf, in_buf, type_size);
	  memcpy(out_buf+type_size, in_buf+(sizes[0]-1)*type_size, type_size);
	  //fprintf(stdout, "%p set to %f from %p - %p set to %f from %p\n", 
	  //		  out_buf, *((float*) out_buf), in_buf,
	  //		  out_buf+type_size, *((float*) out_buf+1), in_buf+(sizes[0]-1)*type_size);
	} else {
	  voxelize(num_dim-1, sizes+1, type_size, in_buf, out_buf);
	  dout = pow(2, num_dim-1)*type_size;
	  din = (sizes[0]-1);
	  for ( i=1 ; i<=(num_dim-1) ; i++)
	  	din = din * sizes[i];
	  din = din * type_size;
	  voxelize(num_dim-1, sizes+1, type_size, in_buf+din, out_buf+dout);
	}

	return 0;
}

//int isInterstingCV(float* values, long size, double epsilon, float *mean_out)
int isInterstingCV(float* values, long size, double epsilon)
{
	
	double mean = 0.0, std_dev = 0.0, coeff_v = 0.0;
	double epsilon_cv = epsilon;
	coeff_v = calculateNCV(values, size, NULL, &std_dev, &mean, NULL);
	//*mean_out = mean;

	// return the result
	if (coeff_v >= epsilon_cv) {
		fprintf(stdout, "YES :: CV = %.5f - std_dev = %.5f - mean= %.5f -- epsilon_cv = %f - ITER: %di\n", coeff_v, std_dev, mean, epsilon_cv, iter);
		return 1;
	} else { 
		fprintf(stdout, "NO :: CV = %.5f - std_dev = %.5f - mean= %.5f -- epsilon_cv = %f - ITER: %di\n", coeff_v, std_dev, mean, epsilon_cv, iter);
		return 0;
	}

}

int smooth_mean(float *data, int size, float mean)
{
	int i = 0;
	
	for ( i=0 ; i<size ; i++){
		data[i] = 0.9*data[i] + 0.1*mean;
	}

	return 0;
}

int isInterstingShanon(float* values, long size, double epsilon)
{
	// calculates the probabilities for the different values
	float* proba = (float*) malloc (sizeof(float)*size);
	float* xi = (float*) malloc (sizeof(float)*size);
	if (proba == NULL) exit(EXIT_FAILURE);
	if (xi == NULL) exit(EXIT_FAILURE);
	long nbrOcc = 0;
	
	int i = 0, j = 0;
	int found = 0;
	// array traversal 
	for ( i=0 ; i<size ; i++){
	  // see if this value is already in the probabilities array
	  found = 0;
	  for ( j=0 ; j<nbrOcc; j++){
	    // a new occuence of th value proba[j][0] is found
	    if (xi[j] == values[i]) {
	    	proba[j]++;
		found = 1;
		break;
	    }
	  }
	  // value[i] is a new value, add it to the proba array
	  if (!found) {
	     xi[nbrOcc] = values[i];
	     proba[nbrOcc] = 1;
	     // increment the number of occurences
	      nbrOcc++;
	  }
	  
	}

	// devide by the number of occurrence to get the proba
	float somme = 0;
	for ( j=0 ; j<nbrOcc ; j++){
	  proba[j] = proba[j]/size;
	  somme += proba[j];
	  //fprintf(stdout, "P(X=%f) = %f\n", xi[j], proba[j]);
	}
	fprintf(stdout, "Somme P(X=xi) = %f\n", somme);
	//*/

	// calculate the Shanon entropy
	double H = 0, epsilon_h = epsilon;
	for ( j=0 ; j<nbrOcc ; j++){
	  H = H - proba[j] * log2(proba[j]);
	}
	
	// normalize H : 0  =< H < ln(n)
	H = H/log2(nbrOcc);

	// test if the source is 
	if ( H < epsilon){
	  fprintf(stdout, "YES :: H = %.5f -- epsilon_h = %f - ITER: %di\n", H, epsilon_h, iter);
	  return 1;
	} else { 
	  fprintf(stdout, "NO :: H = %.5f -- epsilon_h = %f - ITER: %di\n", H, epsilon_h, iter);
	  return 0;
	}

	return 0;
}

int isInterstingShanonV2(float* values, long size, double epsilon)
{
	// calculates the probabilities for the different values
	double* proba = NULL;
	long probaSize = 0;
	calcProbasHist(values, size, INTERVAL_SIZE, 0, &proba, &probaSize);
	// calculate the Shanon entropy
	double H = 0, epsilon_h = epsilon;
	H = calculateShanon(proba, probaSize);
	// normalize H : 0  =< H < ln(n)
	H = H/log2(probaSize);

	// test if the source is 
	if ( H > epsilon_h){
	  fprintf(stdout, "YES :: H (v2) = %.5f -- epsilon_h = %f - ITER: %di\n", H, epsilon_h, iter);
	  return 1;
	} else { 
	  fprintf(stdout, "NO :: H (v2) = %.5f -- epsilon_h = %f - ITER: %di\n", H, epsilon_h, iter);
	  return 0;
	}

	return 0;
}

int quickSort(float* values, long left, long right)
{
	long pIndex = 0;
	if (left < right){
	  //fprintf(stdout, "quickSort: checkpoint 1 - (%ld,%ld)\n", left, right);
	  // partition the array
	  pIndex = quickSortPartition(values, left, right, (left+right)/2); 
	  //fprintf(stdout, "quickSort: checkpoint 2 - (%ld,%ld) pivot %ld\n", left, right, pIndex);
	  // sort smaller values than pivot
	  quickSort(values, left, pIndex-1); // i'm not checking the return value
	  // sort bigger values than pivot
	  quickSort(values, pIndex+1, right); // i'm not checking the return value	
	}

	return 0;
}

long quickSortPartition(float* values, long left, long right, long pIndex){
	  
	long tmpIndex = 0;
	float pValue = 0.0;
	
	long i = 0;
	float tmp = 0.0;
	//fprintf(stdout, "quickSort: checkpoint 1.1 - (%ld,%ld) first pivot index %ld\n", left, right, pIndex);
	// save pivot value
	pValue = values[pIndex];
	// swap pivot value with the last element
	values[pIndex] = values[right];
	values[right] = pValue;
	// partition the array such that values that are less or equal
	// than pivot value should be in the right side of the pivot value
	tmpIndex = left;
	for ( i=left ; i<right ; i++){
	  if (values[i]<=pValue){
	    // swap values[i] with values[tmpIndex]
	    tmp = values[i];
	    values[i] = values[tmpIndex];
	    values[tmpIndex] = tmp;
	    // increment tmpIndex
	    tmpIndex++;
	  }
	}

	// do the last swap
	tmp = values[tmpIndex];
	values[tmpIndex] = values[right];
	values[right] = tmp;

	return tmpIndex;
	
}

// FIXME We cannt know how lonig the probas array size will be
// unless we consider if we 'num' param is set so it will represent the Number_Of_Classes
// Or Maybe we can free the unused space ...
int calcProbasHist(float* values, long size, float intervalSize, long intervalNumber, double** probas, long* num)
{
	float minValue = 0.0, maxValue = 0.0;
	float iSize = 0.0;
	long cNumber = 0;
	long i = 0;

	// get the min and max value
	minValue = values[0];
	maxValue = values[0];
	//fprintf(stdout, "min-value at 0 -is %f - ITER: %d\n", minValue, iter);
	for ( i=0 ; i<size; i++){
	  if (values[i]<minValue){
	    minValue = values[i];
		//fprintf(stdout, "min-value at %d -is %f - ITER: %d\n", i, minValue, iter);
	  }
	  if (values[i]>maxValue)
	    maxValue = values[i];
	}
	fprintf(stdout, "min-value-is %f - ITER: %d\n", minValue, iter);
	fprintf(stdout, "max-value-is %f - ITER: %d\n", maxValue, iter);
	fprintf(stdout, "max-minus-min-is %f - ITER: %d\n", maxValue-minValue, iter);
	/*/
	minValue = values[0];
	maxValue = values[size-1];
	//*/
	
	// if the interval is not set
	if (intervalSize <= 0){
	  if ( intervalNumber > 0)
	    iSize = (maxValue-minValue)/intervalNumber;
	  else
	    iSize = (maxValue-minValue)/INTERVAL_NUMBER_DEFAULT;    
	} else 
	  iSize = intervalSize;
	
	if ( intervalSize > 0) {
	  while (iSize >= (maxValue-minValue))
	    iSize = iSize/2;
	}    
	
	// calculate the probas
	long sizeProbas = 0;
	if (intervalSize > 0) {
	  sizeProbas = (maxValue-minValue)/iSize+1; // +1 is for th max value
	  //if ((iSize*sizeProbas) < (maxValue-minValue))
	  //	sizeProbas++;
	} else if (intervalNumber > 0) {
	  sizeProbas = intervalNumber+1;
	} else {
	  sizeProbas = INTERVAL_NUMBER_DEFAULT+1;
	}

	double* probas_tmp = (double*) malloc (sizeof(double)* sizeProbas);
	// init the array
	for ( i=0 ; i<sizeProbas ; i++){
	  probas_tmp[i] = 0.0;
	}	

	//fprintf(stdout, "Probas array : %p - bins numbers : %ld\n", probas_tmp, sizeProbas);
	//exit(0);	
	
	// calculate the number of occurence
	long counter = 0, index = 0;
	for ( i=0 ; i<size ; i++){
	  index = (values[i]-minValue)/iSize;
	  probas_tmp[index]++;
	  counter++;
	  if ( (index < 0) || (index >= sizeProbas) )
	  	fprintf(stderr, "FIXME: Index Out of Bound 0 <= %d < %d - Debug: value=%f iSize= %f minValue= %f maxValue= %f max-min= %f iSize*sizeProbas=%f\n", index, sizeProbas, values[i], iSize, minValue, maxValue, maxValue-minValue, iSize*sizeProbas);
		//fprintf(stderr, "FIXME:  max-min= %f iSize*sizeProbas=%f\n", maxValue-minValue, iSize*sizeProbas);
	}

	// get the probas
	double sum_debug = 0.0;
	long size_debug = 0;
	//for ( i=0 ; i<(index+1) ; i++){
	for( i=0 ; i<sizeProbas ; i++){
	  size_debug += probas_tmp[i];
	  probas_tmp[i] = probas_tmp[i]/size;
	  sum_debug += probas_tmp[i];
	  //fprintf(stdout, "Probability of the class %ld/%ld:[%f,%f] is %f - sum %f \n", 
	  //                i, sizeProbas, minValue+ i*intervalSize, minValue+ (i+1)*intervalSize, probas_tmp[i], sum_debug);
	  //if (probas[i]== 0) exit(0);
	  if ( (i == (sizeProbas -1) ) && (probas_tmp[i] == 0))
	 	 fprintf(stderr, "FIXME: Extrat bin added 0 <= %d < %d - Debug: value=%f iSize= %f minValue= %f maxValue= %f max-min= %f iSize*sizeProbas=%f\n", index, sizeProbas, values[i], iSize, minValue, maxValue, maxValue-minValue, iSize*sizeProbas);
	}
	
	if (size_debug != size) {
	  fprintf(stdout, "the total number of occurences %ld is different from the real size %ld\n", size_debug, size);
	  exit(0);
	}
	
	*probas = probas_tmp;
	*num = sizeProbas;

	return 0;
}
int calcProbasSum(float* values, long size, double* probas, long* num)
{
	double sum = 0.0;
	if (probas == NULL) exit(EXIT_FAILURE);
	
	long i = 0;
	// clculate the sum of all values
	for ( i=0 ; i<size ; i++){
	  sum += values[i];
	}

	// devide each value by the total sum of all values
	double sum_debug = 0;
	for ( i=0 ; i<size ; i++){
	  probas[i] = values[i]/sum;
	  sum_debug += probas[i];
	  //fprintf(stdout, "P(X=%f) = %f\n", values[i], proba[i]);
	}
	fprintf(stdout, "Somme P(X=xi) = %f\n", sum_debug);
	
	if (num != NULL) *num = size;

	return 0;
}

double calculateShanon(double *probas, long num)
{
	// calculate the Shanon entropy
	//fprintf( stdout, "Calculate Shanon entropy : for |X| = %ld\n", num);
	double H = 0;
	long i = 0;
	for ( i=0 ; i<num ; i++){
	  if (probas[i] != 0)
	    H = H - probas[i] * log2(probas[i]);
	  //fprintf( stdout, "Calculate Shanon entropy : H = %f\n", H);
	}
	
	return H;
}
double calculateTsallis(double *probas, long num, double entropyIndex)
{
	double H = 0.0;
	long i = 0;
	if (entropyIndex == 1) return EXIT_FAILURE;
	
	H = 1.0;
	for ( i=0 ; i<num ; i++){
	  H = H - pow(probas[i], entropyIndex);
	}
	H = H / (1-entropyIndex);

	return H;
}

double calculateNCV(float* values, long size, double* cv, double* stdDev, double *mean, int *normalized)
{
	long i = 0;
	float* vals = values;
	double mean_tmp = 0.0, std_dev = 0.0, coeff_v = 0.0;
	double min_neg_val = 0.0;

	// get the mean
	mean_tmp = 0.0;
	for ( i=0  ; i<size ; i++){
		mean_tmp += vals[i]/size;
		if (vals[i]<min_neg_val)
			min_neg_val = vals[i];
	}
	
	// test if we need a translation
	// to calculate the normalized cv we need to have only positive values
	if (min_neg_val < 0.0){
		mean_tmp = 0.0;
		vals = (float*) malloc (sizeof(float)*size);
		for ( i=0 ; i<size ; i++){
			vals[i] = values[i] - min_neg_val;
			mean_tmp += vals[i]/size; 
		}
	}

	// get the std deviation
	std_dev = 0.0;
	for ( i=0 ; i<size ; i++){
		std_dev += (vals[i]-mean_tmp)*(vals[i]-mean_tmp)/size;
	}
	std_dev = sqrt(std_dev);

	// get the coeffecient of variation
	coeff_v = std_dev/mean_tmp;
	
	if(cv != NULL) *cv = coeff_v;
	if(stdDev != NULL) *stdDev = std_dev;
	if(mean != NULL) *mean = mean_tmp;
	if(normalized != NULL) *normalized = min_neg_val < 0.0? 1 : 0;
	free(vals);

	// return the normalized coeffecient of variation
	//return coeff_v/sqrt(size-1);
	return coeff_v;
}
}
#endif
