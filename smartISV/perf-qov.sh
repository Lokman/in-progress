# generates the Qov vs Performance per iteration comparaison
# Arguments
# $1	statsFile
# $2	outputPrefix

# statsFile sould be in the following format
# perf-file-mean.csv		qov-file.csv	label

function merge-perf-qov.format() {
	perfqovFile=$1
	outPrefix=$2
	
	while read row;
	do
		# merge the perf and qov files
		label=$(echo $row | awk '{print $3}')
		perfFile=$(echo $row | awk '{print $1}')
		qovFile=$(echo $row | awk '{print $2}')
		python  perf-qov.py $perfFile $qovFile 1 \
				> ${outPrefix}_perf-qov-${label}.csv
		paste <(echo "${outPrefix}_perf-qov-${label}.csv") <(echo "${label}")
	done <$perfqovFile	
}


function main() {
	perfqovFile=$1
	outPrefix=$2
	
	merge-perf-qov.format $perfqovFile $outPrefix | tee ${outPrefix}.csv
	Rscript perf-qov.R ${outPrefix}.csv $outPrefix

}

# Check argument
main $1 $2
