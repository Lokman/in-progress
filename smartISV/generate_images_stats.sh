#!/bin/bash
# Generate QoV Stats
# Generates different statistics and graphs from a CM1 reader rendered images

# CMD arguments
# $1    Experiment images files list
# $1    Experiemnt configuration file
# $2    Output prefix




function qov-per-iter.compute () {
# This function compute the QoV of $imagesFile w.r.t a reference one $imgesFileRef
# The QoV is computed using PSNR and SSIM full reference metrics  
# It takes as argument the list of images to compute their QoV
# The generated data format is:
# 	Metric 			iteration-number	QoV
#	PSNR/SSIM0		i	        		XXX01 

    # Arguments
    local imagesFile=${1}
    local compare_images=./compare_images
	local imagesFileRef=imagesFileRef

	# run compare_images program
	for img in $(cat $imagesFile);
	do
		fname=$(basename $img)
		$compare_images $(cat $imagesFileRef | grep $fname) $img > compare.tmp
		cat compare.tmp | grep SSIM | awk '{ print $3 "\t" $4}' | 
						  			  awk -F'.png' '{print $1 $2}' | 
									  awk -F'visit' '{print "SSIM\t" $2}'
		cat compare.tmp | grep PNSR | awk '{ print $3 "\t" $4}' | 
						  			  awk -F'.png' '{print $1 $2}' | 
									  awk -F'visit' '{print "PNSR\t" $2}'
	done
    rm compare.tmp     
}






function per-iter.plot () {
# This function plots multiple stats files in the same figure.
# It uses an R script to generate the figure
# It takes as input  (1) a list of couple filepath-tag (2) a string that will 
# be the name of output figurei (and title)

	# Arguments
	local namedStatsFiles=${1}
	local outputName=${2}

	# plot using R
	Rscript per-iter-geomline.R $namedStatsFiles $outputName

}





function main {
	local imagesFile=${1}
	local outPrefix=${2}

	# agrregate all logs

	qov-per-iter.compute $imagesFile | tee ${outPrefix}_qov-per-iter.csv  
	cat ${outPrefix}_qov-per-iter.csv | grep PNSR | 
					awk '{print $2 "\t" $3}' > ${outPrefix}_pnsr-per-iter.csv
	cat ${outPrefix}_qov-per-iter.csv | grep SSIM |
					awk '{print $2 "\t" $3}' > ${outPrefix}_ssim-per-iter.csv
	
	per-iter.plot ${outPrefix}_pnsr-per-iter.csv ${outPrefix}_pnsr
	per-iter.plot ${outPrefix}_ssim-per-iter.csv ${outPrefix}_ssim
}





# Check CL arguments
#
# Generate stats

main $1 $2 

