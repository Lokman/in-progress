\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{mnotes}[2015/06/09 Meeting Notes class]

%\LoadClass[12pt,a4paper]{article}
\LoadClassWithOptions{article}

% reduce the margins
\usepackage[margin=0.6in]{geometry}

% sans serif font
\renewcommand{\familydefault}{\sfdefault}
%\renewcommand{\sfdefault}{Bitstream-Charter}

% activate hyperlinks
\usepackage{hyperref}

% define personalized commands
\newcommand{\MeetingNotes}[2]{
	\paragraph{Meeting Notes - #1}
	\textbf{\\Kerdatians: #2}
}

