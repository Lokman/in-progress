#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <Damaris.h>
#include <unistd.h>
//#include "filter.c"
#include "sobel3d.c"

extern int ni, nj, nk; 	// dimensions of a chunk in each process
extern float* dbz; 	// reflectivity (Decibel Z)
extern float* uinterp; 	// interpolated velocity along u axis (m/s)
extern float* vinterp; 	// interpoloated velocity along v axis (m/s)
extern float* xf;
extern float* yf;
extern float* zf;

extern int iter;

void execute_one_timestep(MPI_Comm comm, int step)
{
	int rank;
	MPI_Comm_rank(comm, &rank);

	damaris_parameter_set("NI", &ni, sizeof(int));
	damaris_parameter_set("NJ", &nj, sizeof(int));
	damaris_parameter_set("NK", &nk, sizeof(int));

	damaris_write("fields/uinterp", uinterp);
	
	damaris_write("coordinates/xf", xf);
	damaris_write("coordinates/yf", yf);
	damaris_write("coordinates/zf", zf);

	damaris_signal("write_cv");

	damaris_end_iteration();
	
	static int first_call = 1;
	if(first_call) {
		if(rank == 0){
			//int i = 0;
			//while (!damaris_vclient_connected()){
			fprintf(stderr, "waiting for Visit client connection ...\n");
			//sleep(5);
			//	//if (i++>10) break;
			//}
			fprintf(stderr,"Waiting input from user...");
			//int wait;
			//scanf("%d", &wait);
		}
		char c= getchar();
		MPI_Barrier(comm);
		first_call = 0;
	}

	sleep(1);
}

