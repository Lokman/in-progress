#!/usr/bin/python

import sys
import csv 

# For the computation of the quartiles 
#import numpy as np

# Or, a pure pyhton implementation to compute percentiles
# cf. percentile(N, percent, key=lambda x:x)
import math
import functools

# open the * per iteration file and comupte for each 
with open(sys.argv[1]) as perfFile:
    perfCSV = csv.reader(perfFile, delimiter='\t');
    # stats aggregates in alist all values per iteration
    # means collecte the mean of values per iteration
    stats = dict()
    means = dict()
    for row in perfCSV:
        if row[0] in stats:
            stats[row[0]].append(row[1])
        else:
            stats[row[0]] = []
        if row[0] in means:
            means[row[0]][0] += float(row[1])
            means[row[0]][1] += int(1)
        else:
            means[row[0]] = [float(row[1]),int(1)]

# Pure-python implementation of percentiles function
# src: http://code.activestate.com/recipes/511478-finding-the-percentile-of-the-values/ 
# modified version
def percentile(N, percent, key=lambda x:x):
    """
    Find the percentile of a list of values.

    @parameter N - is a list of values. Note N MUST BE already sorted.
    @parameter percent - a float value from 0.0 to 1.0.
    @parameter key - optional key function to compute value from each element of N.

    @return - the percentile of the values
    """
    if not N:
        return None
    k = (len(N)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return key(N[int(k)])
    d0 = float(N[int(f)]) * (c-k)
    d1 = float(N[int(c)]) * (k-f)
    return d0+d1

# compute quartiles
# and means
print 'iteration\tmin\t1stQT\t2ndQT\t3rdQT\tmax\tmean'.format(iterNum, 
for iterNum in stats.keys():
    stats[iterNum].sort()
    #len(stats[iterNum])
    #print means[iterNum]
    print '{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(iterNum, 
                                         percentile(stats[iterNum], 0.00), 
                                         percentile(stats[iterNum], 0.25), 
                                         percentile(stats[iterNum], 0.50), 
                                         percentile(stats[iterNum], 0.75),
                                         percentile(stats[iterNum], 1.00),
                                         means[iterNum][0]/means[iterNum][1])

    

