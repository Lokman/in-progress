#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <mpi.h>
#include <hdf5.h>
#include <H5LTpublic.h>
#include "Damaris.h"
#include "chunk.c"

int ni, nj, nk; // dimensions per process
float* dbz = NULL; 
float* uinterp = NULL; 
float* vinterp = NULL; // datasets

float* xf;
float* yf;
float* zf;

int iter = 0; // current iteration no

int* iterations; // available iterations
int num_iterations; // number of iterations
char basename_[256]; // directory in which to take datasets

void execute_one_timestep(MPI_Comm comm, int step);

int read_datasets(char* dset, int i, int rank, float** ptr)
{
	int shared_rank = rank/3;
	if(rank == 0)
		printf("Reading dataset %d\n",i);
	char filename[256];
	sprintf(filename,"%s/%s/%05d/%06d.h5",
		basename_,dset,i,shared_rank);

	if(rank == 0)
		printf("Opening file %s\n",filename);
	hid_t fp = H5Fopen(filename,H5F_ACC_RDONLY,H5P_DEFAULT);
	if(fp < 0) return -1;

	int dim;
	H5LTget_dataset_ndims(fp,dset,&dim);
	hsize_t* dims = (hsize_t*)malloc(sizeof(hsize_t)*dim);
	size_t type_size;
	H5T_class_t class_id;
	H5LTget_dataset_info(fp,dset, dims, &class_id, &type_size);

	// assume 3 dimensions and all variables have the same extents
	ni = dims[2];
	nj = dims[1];
	nk = dims[0];
	
	// Array Chunk sizes
	int chunk_sizes[3] = {0};
	chunk_sizes[0] = dims[0];
	//chunk_sizes[1] = (rank%3==2)? dims[1]/3+dims[1]%3:dims[1]/3;
	chunk_sizes[1] = dims[1]/3;
	chunk_sizes[2] = dims[2];
	int chunk_array_size = chunk_sizes[0]*chunk_sizes[1]*chunk_sizes[2];
	// Offsets to get each chunk
	int offsets[3] = {0};
	offsets[0] = 0;
	offsets[1] = (dims[1]/3)*(rank%3);
	offsets[2] = 0;

	//allocate mem
	float* ptr_all = (float*) malloc(type_size*ni*nj*nk);
	if(*ptr == NULL)
		*ptr = (float*) malloc(type_size*chunk_array_size);
	
	// read the whole array
	H5LTread_dataset_float(fp,dset, ptr_all);
	
	// get the chunk of this process
	int j = 0;
	char * all = (char*) ptr_all;
	char * tmp = (char*) *ptr;
	char * source = NULL;
	int chunk_size = 0;
	for ( j=0 ; j<(dims[0]*dims[1]*dims[2]) ; j=j+(dims[1]*dims[2]) ){
		source = all+j*type_size+type_size*chunk_sizes[1]*chunk_sizes[2]*(rank%3);
		//if ( ((int) source) > ((int) &(larray[l-1]))){
		//	fprintf(stderr, "Pointer Out Of Bound - Source\n");
		//	//exit(0);
		//	break;
		//}
	        chunk_size = chunk_sizes[1]*chunk_sizes[2]*type_size;
		//if (((int) tmp)>( ((int)ptr) + type_size*chunk_sizes[0]*chunk_sizes[1]*chunk_sizes[2])){
		//	fprintf(stderr, "Index Our Of Bound - Destination\n");
		//	//exit(-1);
		//	break;
		//}
		memcpy( tmp, source, chunk_size);
		tmp = tmp + chunk_sizes[1]*chunk_sizes[2]*type_size;
	}
	
	ni = chunk_sizes[2];
	nj = chunk_sizes[1];
	nk = chunk_sizes[0];

	free(dims);

	H5Fclose(fp);
	return 0;
}

int compare_ints(const void* a, const void* b)
{
	const int* ia = (const int*)a;
	const int* ib = (const int*)b;
	return (*ia > *ib) - (*ia < *ib);
}

void list_iterations(char* dirname)
{
	DIR* dp;
	struct dirent *ep;
	char* mydir = (char*) malloc(sizeof(char)*(5+strlen(dirname)));
	sprintf(mydir,"%s/dbz",dirname);
	dp = opendir(mydir);
	if(dp == NULL) {
		fprintf(stderr,"ERROR: cannot open directory %s.",mydir);
		MPI_Abort(MPI_COMM_WORLD,911);
	}
	
	num_iterations = 0;
	while(ep = readdir(dp)) {
		if(strcmp(ep->d_name,".") != 0
		&& strcmp(ep->d_name,"..") != 0) {
			num_iterations++;
		}
	}
	closedir(dp);

	iterations = (int*) malloc(sizeof(int)*num_iterations);
	dp = opendir(mydir);
	int i = 0;
	while(ep = readdir(dp)) {
		if(strcmp(ep->d_name,".") != 0
		&& strcmp(ep->d_name,"..") != 0) {
			iterations[i] = atoi(ep->d_name);
			i++;
		}
	}
	closedir(dp);
	qsort (iterations,num_iterations,sizeof(int),compare_ints);
  // shortcut to play only the last 5 iterations
  iterations = &(iterations[15]);
  num_iterations = 8;
  //*/
}

void do_iteration(MPI_Comm comm, int i)
{
	int rank, err;
	MPI_Comm_rank(comm,&rank);
	err = read_datasets("dbz",i,rank,&dbz);
	if(err) MPI_Abort(MPI_COMM_WORLD,911);
	err = read_datasets("uinterp",i,rank,&uinterp);
	if(err) MPI_Abort(MPI_COMM_WORLD,911);
	err = read_datasets("vinterp",i,rank,&vinterp);
	if(err) MPI_Abort(MPI_COMM_WORLD,911);
	// do something with the datasets here
	if(xf != NULL) {
		free(xf); free(yf); free(zf);
	}

	xf = (float*) malloc(sizeof(float)*ni);
	yf = (float*) malloc(sizeof(float)*nj);
	zf = (float*) malloc(sizeof(float)*nk);

	int offset_x = ((rank/3)%16)*6000 - 48000;
	int offset_y = ((rank/3)/16)*6000 - 48000+(rank%3)*nj*100;
	int offset_z = 0;

  offset_x = 0; offset_y = 0; offset_z = 0;

	int j;
	for(j=0; j<ni; j++) xf[j] = (float)(offset_x + j*100);
	for(j=0; j<nj; j++) yf[j] = (float)(offset_y + j*100);//-rank*8.5;
	for(j=0; j<nk; j++) zf[j] = (float)(offset_z + j*200);

	execute_one_timestep(comm, i);
}

int main(int argc, char** argv)
{
	int rank, size;
	MPI_Init(&argc,&argv);
	MPI_Comm comm = MPI_COMM_WORLD;

	MPI_Comm_rank(comm, &rank);
	MPI_Comm_size(comm, &size);

	char* configfile = NULL;

	// reading the configuration from the current directory
	if(rank == 0) {
		if(argc == 1) MPI_Abort(MPI_COMM_WORLD,911);
		strcpy(basename_,".");
		if(argc > 2) {
			strcpy(basename_,argv[2]);
		}
		list_iterations(basename_);
	}

	configfile = argv[1];

	// broadcasting configuration to other processes
	MPI_Bcast(&num_iterations,1,MPI_INT,0,comm);
	if(rank != 0)
		iterations = (int*)malloc(sizeof(int)*num_iterations);
	MPI_Bcast(iterations,num_iterations,MPI_INT,0,comm);
	MPI_Bcast(basename_,256,MPI_BYTE,0,comm);

	damaris_initialize(configfile,comm);

	int is_client;
	int err = damaris_start(&is_client);

	if(is_client && (err == DAMARIS_OK || err == DAMARIS_NO_SERVER)) {
		// loop over iterations
		damaris_client_comm_get(&comm);
		int i;
		for(i=0; i<num_iterations; i++)
		{
			iter = i;
			MPI_Barrier(comm);
			do_iteration(comm,iterations[i]);
		}

		damaris_stop();
	}

	damaris_finalize();

	MPI_Finalize();
	return 0;
}






