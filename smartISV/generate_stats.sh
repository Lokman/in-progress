#!/bin/bash
# Generate Stats
# Generates different statistics and graphs from a CM1 reader log file
# 1- Applys *.format scripts: extract raw stats from the log file
#  - The names of output files may contain information about the run parameters,
#    for instance: The selection metric, the threshold. 
# 2- Applys summarizing scripts: compute the mean, the 1st,3rd quartile, ...
# 3- Applys aggregation scripts: for stats that require data from multiple runs


# CMD arguments
# $1    Experiment logs files list
# $1    Experiemnt configuration file
# $2    Output prefix




function perf-per-core-per-iter.format () {
# This function extract time took to visualize the data per iteration for each 
# core. 
# The keyword used is "time-took", that should appear in simulation logs.
# It takes as argument the number of dedicated core (this arg is needed because
# dedicated cores logs don't include, yet, the iteration number).
# The generated data format is:
# 	iteration-number	time-to-visualize ttv (in seconds)
#	0			        XXX01 
#	0			        XXX02
#	. 			        .
#	.			        .
#	i			        XXXi1

    # Arguments
    local logFile=${1}
    local dedicatedCoresNumber=${2}

    #echo "iteration_number	time_took_to_visualize"
    cat $logFile | grep time-took > _tmp_perf_per_iter
    # add header
    #echo "iter	perf"
    cat -n _tmp_perf_per_iter | awk -v coresNum="${dedicatedCoresNumber}" '{ \
	    if (int($1/coresNum) == ($1/coresNum))  \
		    print int($1/coresNum)-1 "\t"  $3;  \
		else                                    \
			print int($1/coresNum)   "\t"  $3;  \
		}'
    rm _tmp_perf_per_iter     
}





function cov-per-block-per-iter.format () {
# This functiuon extracts the value of each block's coefficient-of-variation
# per iteration.
# The keyword used is "CV", that should appear in the simulation logs.
# It has no extra arguments
# The generated data format is:
# 	iteration_number	CV
#	0			        CV01 
#	0			        CV02
#	. 			        .
#	.			        .
#	i			        CVi1
    
    # Arguments
    local logFile=${1}

    # add header
	#echo "iteration	 cov"
    cat $logFile | grep "CV" | awk '{ print $19 "\t" $5}' | 
                               awk -F'i' '{ print $1 $2}'
}





function entropy-per-block-per-iter.format () {
# This function extracts the value of each block's entropy per iteration
# The keyword used is "H (v2)", that should appear in the simulation logs
# It has no extra arguments
# The generated data format is:
# 	iteration-number	Normalized-Entropy
#	0			        Entr01 
#	0			        Entr02
#	. 			        .
#	.			        .
#	i			        Entri1

    # Arguments
    local logFile=${1}

	# add header
    #echo "iter	n-entropy"
    cat $logFile | grep "H (v2)" | awk '{ print $13 "\t" $6}' | 
                             awk -F'i' '{ print $1 $2}'
}





function perf-per-iter.summary () {
# This function summarizes the performance per iterarion.
# The input format is: iteration-number(1st colummn) perf(2nd column) 
# It has no extra arguments
# The generated data format is:
#  iter-number     min-ttv   1stQT-ttv   mean-ttv  3rdQT-ttv   max-ttv
#  0               XXXXX0
#  iMax            XXXXXiMax

    # Arguments
    local perfFile=${1}

    python per-iter-summary.py $perfFile
}





function cov-per-iter.summary () {
# This function summarizes the blocks cov per iterarion.
# The input format is: iteration-number(1st colummn) cov(2nd column) 
# It has no extra arguments
# The generated data format is:
#  iter-number     min-cov   mean-cov   max-cov   1stQT-cov  3rdQT-cov
#  0               XXXXX0
#  iMax            XXXXXiMax
# TODO: compute 1stQt, 2ndQt(mediane) and the 3rdQt

    # Arguments
    local covFile=${1}

    python per-iter-summary.py $covFile
}





function entropy-per-iter.summary () {
# This function summarizes the blocks entropy per iterarion.
# The input format is: iteration-number(1st colummn) entropy(2nd column) 
# It has no extra arguments
# The generated data format is:
#  iter-number min-entropy mean-entropy max-entropy 1stQT-entropy 3rdQT-entropy
#  0            XXXXX0
#  iMax         XXXXXiMax
# TODO: compute 1stQt, 2ndQt(mediane) and the 3rdQt

    # Arguments
    local entropyFile=${1}

    python per-iter-summary.py $entropyFile
}





function perf-per-core-per-iter.aggregate () {
# This functiuon aggregates the time took to visualize per core per iteration, 
# for multiple runs with the SAME config.
# It has no extra arguments
# The generated data format is:
# 	iteration_number	time_took_to_visualize
#	0			        T01 
#	0			        T02
#	. 			        .
#	.			        .
#	i			        Ti1
    
    # Arguments
    local logFiles=${1}
	local dedicatedNum=${2}

    # add header
	#echo "iteration	time_took_to_visualize"
	for file in $(cat $logFiles);
	do
		# skip the header
		# tail -n +2 "$file"
		#echo "processing file: ${file}"
		perf-per-core-per-iter.format $file $dedicatedNum
	done
}





function cov-per-block-per-iter.aggregate () {
# This functiuon aggregates the stats of blocks coefficient-of-variation
# per iteration, for multiple runs with the SAME config.
# It has no extra arguments
# The generated data format is:
# 	iteration_number	CV
#	0			        CV01 
#	0			        CV02
#	. 			        .
#	.			        .
#	i			        CVi1
    
    # Arguments
    local statsFiles=${1}

    # add header
	#echo "iteration	 cov"
	for file in $(cat $statsFiles);
	do
		# skip header
		#tail -n +2 "$file"
		#echo "processing file: ${file}"	
		cov-per-block-per-iter.format  $file
	done
}





function entropy-per-block-per-iter.aggregate () {
# This function aggregates the stats of blocks entropy per iteration, 
# for multiple runs with the SAME config.
# It has no extra arguments
# The generated data format is:
# 	iteration-number	Normalized-Entropy
#	0			        Entr01 
#	0			        Entr02
#	. 			        .
#	.			        .
#	i			        Entri1

    # Arguments
    local statsFiles=${1}

	# add header
    #echo "iter	n-entropy"
    
	for file in $(cat $statsFiles);
	do
		# skip header
		#tail -n +2 "$file"
		entropy-per-block-per-iter.format $file
	done
}






function per-iter.plot () {
# This function plots multiple stats files in the same figure.
# It uses an R script to generate the figure
# It takes as input  (1) a list of couple filepath-tag (2) a string that will 
# be the name of output figurei (and title)

	# Arguments
	local namedStatsFiles=${1}
	local outputName=${2}

	# plot using R
	Rscript per-iter-quartiles-boxplot.R $namedStatsFiles $outputName
	Rscript per-iter-min-max-geomline.R $namedStatsFiles $outputName

}





function main {
	local logFiles=${1}
	local outPrefix=${2}

	# agrregate all logs

	perf-per-core-per-iter.aggregate $logFiles 30 | tee ${outPrefix}_perf-stats.csv
	perf-per-iter.summary ${outPrefix}_perf-stats.csv | tee ${outPrefix}_perf-stats-summary.csv
	per-iter.plot ${outPrefix}_perf-stats.csv ${outPrefix}_perf 

	#cov-per-block-per-iter.aggregate $logFiles > ${outPrefix}_cov-stats.csv
	#cov-per-iter.summary ${outPrefix}_cov-stats.csv > ${outPrefix}_cov-stats-summary.csv
	#per-iter.plot ${outPrefix}_cov-stats.csv ${outPrefix}_cov

	#entropy-per-block-per-iter.aggregate $logFiles > ${outPrefix}_entropy-stats.csv
	#entropy-per-iter.summary ${outPrefix}_entropy-stats.csv > ${outPrefix}_entropy-stats-summary.csv
	#per-iter.plot ${outPrefix}_entropy-stats.csv ${outPrefix}_entropy
}





# Check CL arguments
#
# Generate stats

main $1 $2 

#
