/*
Copyright (c) 2013, Matthieu Dorier (matthieu.dorier at irisa.fr)
All rights reserved.
 
Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this 
list of conditions and the following disclaimer in the documentation and/or 
other materials provided with the distribution.
Neither the name of the <ORGANIZATION> nor the names of its contributors may be 
used to endorse or promote products derived from this software without specific 
prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON 
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __CHK_H
#define __CHK_H

#include <stdlib.h>

/**
 * Checks if a chunk is included within a big multi-dim array.
 * dim : number of dimensions
 * size_big : array of sizes of the big array (in C order)
 * offsets : array of offsets defining the chunk (in C order)
 * size_small : array of sizes of the small chunk (in C order)
 * returns 1 if the chunk is included in the array, 0 otherwise.
 */
int chk_includes(unsigned short dim, const int* size_big,
		const int* offsets, const int* size_small);

/**
 * Extract a chunk from a big multi-dim array.
 * big : pointer to the big array
 * small : pointer to the chunk (memory must be allocated)
 * dim : number of dimensions
 * offsets : offsets at which the chunk starts in the big array
 * size_big : array of sizes of the big array
 * size_small : array of sizes of the small array
 * elem_size : size of a single element (in bytes)
 */
int chk_extract(const char* big, char* small,
		unsigned short dim, const int* offsets,
		const int* size_big, const int* size_small,
		size_t elem_size);

/**
 * The the opposite than chk_extract: copies the content of
 * the chunk into the bigger array at a specified location.
 */
int chk_insert(char* big, const char* small,
		unsigned short dim, const int* offsets,
		const int* size_big, const int* size_small,
		size_t elem_size);
#endif
