#include <damaris/data/VariableManager.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Damaris.h"
#include "filter.c"

USING_POINTERS;
using namespace damaris;

extern "C" {	

// function called as a Damaris plugin
void smartReduction(const char* /*name*/, int source, int iteration,
		    const char* /*args*/) {
  
	fprintf(stdout, "SmartViz plugin executing here! ==================\n");
  
	// Check if the plugin is activated
	int enableSmartViz = 1;
	if(damaris_parameter_get("SmartViz_On", &enableSmartViz, sizeof(int))
	   != DAMARIS_OK){
		enableSmartViz = 1;
		fprintf(stderr, "SmartViz: SmartViz_on non set - default: active\n");
	}
	if (!enableSmartViz)
		return;
  
	// Get SmartViz parameter
	float threshold = 0.0; // selection threshold
	if(damaris_parameter_get("SmartViz_Threshold", &threshold, sizeof(float))
	   != DAMARIS_OK){
		threshold = 0.65;
		fprintf(stderr, "SmartViz: SmartViz_Threshold non set - default: %f\n", threshold);
	}
	int useGradient = 0; // compute the derivates of each datablock
	if(damaris_parameter_get("SmartViz_Gradient", &useGradient, sizeof(int))
	   != DAMARIS_OK)
		useGradient = 0;
	int lowerAll = 0; // lower all the blocks resolution 
	if(damaris_parameter_get("SmartViz_Low", &lowerAll, sizeof(int))
	   != DAMARIS_OK)
		lowerAll = 0;
	int metric = 0; // selection metric: 0 -> CoV, 1 -> Entropy 
	// TODO: change metric type to string, it'll be more readable
	if(damaris_parameter_get("SmartViz_Metric", &metric, sizeof(int))
	   != DAMARIS_OK)
		metric = 0; 

	// Get the simulation variable
	// TODO: get the variable name from a SmartViz parameter
	shared_ptr<Variable> uinterp = VariableManager::Search("fields/uinterp");
	if(!uinterp){
		fprintf(stderr, "SmartViz: field 'uinterp' not found\n"); 
    		return;
	}
  
	// Get the block by source and iteration	
	// TODO: process multiple blocks per domain
	shared_ptr<Block> b = uinterp->GetBlock(source, iteration, 0);
	if(!b){ 
		fprintf(stderr, "SmartViz: block (%d,%d,%d) not found\n",
			source, iteration, 0);
		return;
	}
	
	// Get block's buffer
	float* data = (float*) b->GetDataSpace().GetData();
	
	
	//printf("block-found, writable = %d, nbre of items= %d, nbre of dimensions= %d\n",
	//     not b->IsReadOnly(), b->NbrOfItems(), b->GetDimensions()); 
	//printf("block start indexs [%d,%d,%d] end indexes[%d,%d,%d]\n",
	//       b->GetStartIndex(0), b->GetStartIndex(1), b->GetStartIndex(2),
	//       b->GetEndIndex(0), b->GetEndIndex(1), b->GetEndIndex(2));
	//printf("Is intersting cv ? %d\n", 
	//isInterstingCV((float*)(b->GetDataSpace().GetData()), 
	//		b->NbrOfItems(), threshold));
	//for (int i = 0; i<b->NbrOfItems(); i++)
	//	printf(" data[%d]=%f ", ((float* )(b->GetDataSpace().GetData()))[i]);
	//printf("\n");
	// If the data block is not intersting lower its resoltion

	if (((metric == 0) && 
	     !isInterstingCV(data, b->NbrOfItems(), threshold)) ||
	    ((metric == 1) && 
	     !isInterstingShanonV2(data, b->NbrOfItems(), threshold))){
		// reduce the bloc to 2x2x2 extents
		int ni=2,nj=2,nk=2;
		int nbrItems = ni*nj*nk;
		int sizes[3];
		//sizes[0] = b->GetEndIndex(0) - b->GetStartIndex(0);
		//sizes[1] = b->GetEndIndex(1) - b->GetStartIndex(1);
		//sizes[2] = b->GetEndIndex(2) - b->GetStartIndex(2);
		damaris_parameter_get("NK", &sizes[0], sizeof(int));
		damaris_parameter_get("NY", &sizes[1], sizeof(int));
		damaris_parameter_get("NI", &sizes[2], sizeof(int));

		// lower the data resolution
		float* newData = (float*) malloc(nbrItems*sizeof(float));
		//voxelize(b->GetDimensions(), sizes, sizeof(float), (char*) data, (char*) buffer);
		voxelize(3, sizes, sizeof(float), (char*) data, (char*) newData);

		// delete the full resolution block to create a low resolution one
		// TODO: Is it possible to change the bounds and data without having to
		// delete and recreate a new one?
		uinterp->DetachBlock(b); //TODO: check if the memory was freed
		int64_t lbounds[3],ubounds[3];
		lbounds[0] = 0; lbounds [1] = 0; lbounds [2] = 0;
		ubounds[0] = 1; ubounds [1] = 1; ubounds [2] = 1;
		//shared_ptr<Block> newb = uinterp->Allocate(source, iteration, 0, true);
		shared_ptr<Block> newb = uinterp->AllocateFixedSize(source, iteration, 0,
					   lbounds, ubounds, true); //TODO: is blocking needed
		// inspired from client/Client.cpp:Client::Write
		// uinterp->DetachBlock(newb);
		DataSpace<Buffer> ds = newb->GetDataSpace();
		//newb->LoseDataOwnership();
		void* buffer = ds.GetData();
		if(buffer == NULL) {
			fprintf(stderr, "SmartViz: DataSpace error when reducing block resolution\n");
			//uinterp->AttachBlock(b);
			return;
		}
		//size_t size = ds.GetSize();
		memcpy(buffer, newData, nbrItems*sizeof(float));
		// TODO: free the original bloc
	} 
  
	fprintf(stdout, "SmartViz plugin done! ==============================\n");
}

}

