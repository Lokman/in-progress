import sys
import csv

# Check arguments
if len(sys.argv) != 4:
	print 'usage: {} <file1> <file2> <merge column number - starts from 1>'.format(sys.argv[0])
	sys.exit()


handles = [open(filename) for filename in sys.argv[1:3]]    
readers = [csv.reader(f, delimiter='\t') for f in handles]
joinCol = int(sys.argv[3])-1

merged = dict()
for i in range(len(readers)):
	for row in readers[i]:
		key = int(row[joinCol])
		if key in merged:
			merged[key] += row[:joinCol] + row[joinCol+1:]
			#print 'key {} appended --value--> {}'.format(key, merged[key])
		else: 
			merged[key] = row[:joinCol]+ row[joinCol+1:]
			#print 'new key {} created --value--> {}'.format(key, merged[key])

# print results
for key in merged.keys():
	# tmp until I fix the missing visit iteration image
	if key == 22: continue
	line = ''
	for col in merged[key]:
		line += '{}\t'.format(col)
	print line+str(key)
			
			
	
		
	

