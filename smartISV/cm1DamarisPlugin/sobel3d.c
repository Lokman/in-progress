#ifndef DEF_SOBEL_3D
#define DEF_SOBEL_3D
#include <math.h>

float get3dValue(float* array, int coord_1, int coord_2, int coord_3, int dim_1, int dim_2, int dim_3);
float set3dValue(float* array, float value, int coord_1, int coord_2, int coord_3, int dim_1, int dim_2, int dim_3);


/* FIXME: Now its only 2D, I need to say how to generalize it  */
int sobel3d(float* image, int* size, float** image_gradient, MPI_Comm comm_debug)
{
	if (*image_gradient == NULL)
		*image_gradient = (float*) malloc(size[0]*size[1]*size[2]*sizeof(float));
	
	int rank = -1;
	//MPI_Comm_rank(comm_debug, &rank);

	float sobel_x[3][3] = {{1,0,-1},{2,0,-2},{1,0,-1}};
	float sobel_y[3][3] = {{1,2,1},{0,0,0},{-1,-2,-1}};
	
	int k = 0, j = 0, i = 0;
	float val_x = 0.0, val_y = 0.0, val = 0.0;
	//FIXME: As it's just 2d filter i will travers each z value
	for(k=0; k<size[0]; k++){
	   // for each value apply the sobel kernel
	   for ( i=0 ; i<size[2] ; i++){
	      for ( j=0 ; j<size[1] ; j++){
			// set the corner values, as the gradient cannot be computed for them 
	        if ((i==0) || (j==0) || (i==(size[2]-1)) || (j==(size[1]-1))){
		      //set3dValue(*image_gradient, get3dValue(image, k, j, i, size[0], size[1], size[2]), k, j, i, size[0], size[1], size[2]);
		      set3dValue(*image_gradient, 0, k, j, i, size[0], size[1], size[2]);
	          continue;
		 	}

	         val_x = sobel_x[0][0]*get3dValue(image, k, j-1, i-1, size[0], size[1], size[2])+
		         sobel_x[0][1]*get3dValue(image, k, j, i-1, size[0], size[1], size[2])+
		         sobel_x[0][2]*get3dValue(image, k, j+1, i-1, size[0], size[1], size[2])+
		         sobel_x[1][0]*get3dValue(image, k, j-1, i, size[0], size[1], size[2])+
		         sobel_x[1][1]*get3dValue(image, k, j, i, size[0], size[1], size[2])+
		         sobel_x[1][2]*get3dValue(image, k, j+1, i, size[0], size[1], size[2])+
		         sobel_x[2][0]*get3dValue(image, k, j-1, i+1, size[0], size[1], size[2])+
		         sobel_x[2][1]*get3dValue(image, k, j, i+1, size[0], size[1], size[2])+
		         sobel_x[2][2]*get3dValue(image, k, j+1, i+1, size[0], size[1], size[2]);
	         
		 	val_y = sobel_y[0][0]*get3dValue(image, k, j-1, i-1, size[0], size[1], size[2])+
		         sobel_y[0][1]*get3dValue(image, k, j, i-1, size[0], size[1], size[2])+
		         sobel_y[0][2]*get3dValue(image, k, j+1, i-1, size[0], size[1], size[2])+
		         sobel_y[1][0]*get3dValue(image, k, j-1, i, size[0], size[1], size[2])+
		         sobel_y[1][1]*get3dValue(image, k, j, i, size[0], size[1], size[2])+
		         sobel_y[1][2]*get3dValue(image, k, j+1, i, size[0], size[1], size[2])+
		         sobel_y[2][0]*get3dValue(image, k, j-1, i+1, size[0], size[1], size[2])+
		         sobel_y[2][1]*get3dValue(image, k, j, i+1, size[0], size[1], size[2])+
		         sobel_y[2][2]*get3dValue(image, k, j+1, i+1, size[0], size[1], size[2]);
		
		val = sqrt((val_x*val_x) + (val_y*val_y));
		set3dValue(*image_gradient, val, k, j, i, size[0], size[1], size[2]);
		//if (rank == 10){
		//	fprintf(stdout, "Writing Gradient value : %f - %f\n", val, get3dValue(*image_gradient, k, j, i, size[0], size[1], size[2]));
		//}
	      }
	   }
	}

	//memcpy( );

	return 0;
}

float get3dValue(float* array, int coord_1, int coord_2, int coord_3, int dim_1, int dim_2, int dim_3)
{
        long index = coord_1*dim_2*dim_3+coord_2*dim_3+coord_3;
        return array[index];
}
float set3dValue(float* array, float value, int coord_1, int coord_2, int coord_3, int dim_1, int dim_2, int dim_3)
{
         long index = coord_1*dim_2*dim_3+coord_2*dim_3+coord_3;
         array[index] = value;
         return array[index];
}

#endif
