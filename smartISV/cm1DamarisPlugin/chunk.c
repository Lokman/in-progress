#include <stdlib.h>
#include <string.h>
//#ifdef HAS_MPI
//	#include <mpi.h>
//#endif

#include "chunk.h"

int chk_includes(unsigned short dim, const int* size_big,
		const int* offsets, const int* size_small)
{
	int b = 1;
	int i;
	for(i=0; i<dim; i++) {
		b = b && (offsets[i] >= 0)
		      && (offsets[i] < size_big[i])
		      && (size_small[i] > 0)
		      && (offsets[i] + size_small[i] <= size_big[i]);
		if(!b) break;
	}
	return b;
}

int chk_extract(const char* big, char* small,
		unsigned short dim, const int* offsets,
		const int* size_big, const int* size_small,
		size_t elem_size)
{
#ifdef HAS_MPI_DONT_USE
	int count = elem_size, i;
	for(i=0; i<dim; i++) {
		count *= size_small[i];
	}
	MPI_Datatype elem;
	MPI_Type_contiguous(elem_size,MPI_CHAR,&elem);
	MPI_Datatype cube;
	MPI_Type_create_subarray(dim,(int*)size_big,(int*)size_small,
				 (int*)offsets,MPI_ORDER_C,elem,&cube);
	MPI_Type_commit(&cube);
	MPI_Request request;
	MPI_Irecv(small,count,MPI_CHAR,0,0,MPI_COMM_SELF,&request);
	MPI_Send((char*)big,1,cube,0,0,MPI_COMM_SELF);
	MPI_Wait(&request,MPI_STATUS_IGNORE);
	MPI_Type_free(&cube);
	return 1;
#else
	if(dim == 1) {
		memcpy(small, big+offsets[0]*elem_size, 
		       size_small[0]*elem_size);
	} else {
		int i;
		size_t slice_big = elem_size;
		size_t slice_small = elem_size;
		for(i=1; i<dim; i++) {
			slice_big *= size_big[i];
			slice_small *= size_small[i];
		}
		big += slice_big*offsets[0];
		for(i=0; i<size_small[0]; i++) {
			chk_extract(big + i*slice_big,
				    small + i*slice_small,
				    dim-1, offsets+1,
				    size_big+1, size_small+1, elem_size);
		}
	}
	return 1;
#endif
}

int chk_insert(char* big, const char* small,
		unsigned short dim, const int* offsets,
		const int* size_big, const int* size_small,
		size_t elem_size)
{
#ifdef HAS_MPI
        int count = elem_size, i;
        for(i=0; i<dim; i++) {
                count *= size_small[i];
        }
        MPI_Datatype elem;
        MPI_Type_contiguous(elem_size,MPI_CHAR,&elem);
        MPI_Datatype cube;
        MPI_Type_create_subarray(dim,(int*)size_big,(int*)size_small,
                                 (int*)offsets,MPI_ORDER_C,elem,&cube);
        MPI_Type_commit(&cube);
	MPI_Request request;
	MPI_Irecv(big,1,cube,0,0,MPI_COMM_SELF,&request);
        MPI_Send((char*)small,count,MPI_CHAR,0,0,MPI_COMM_SELF);
        MPI_Wait(&request,MPI_STATUS_IGNORE);
	MPI_Type_free(&cube);
        return 1;
#else
	if(dim == 1) {
		memcpy(big+offsets[0]*elem_size, small, 
		       size_small[0]*elem_size);
	} else {
		int i;
		size_t slice_big = elem_size;
		size_t slice_small = elem_size;
		for(i=1; i<dim; i++) {
			slice_big *= size_big[i];
			slice_small *= size_small[i];
		}
		big += slice_big*offsets[0];
		for(i=0; i<size_small[0]; i++) {
			chk_insert(big + i*slice_big,
				   small + i*slice_small,
				   dim-1, offsets+1,
				   size_big+1, size_small+1, elem_size);
		}
	}
	return 1;
#endif
}
